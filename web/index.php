<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;


$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());



$app->get('/', function () use ($app) {
    return $app['twig']->render('home.twig');
})->bind('home');

$app->get('/preise', function () use ($app) {
    return $app['twig']->render('preise.twig');
})->bind('preise');

$app->get('/produkte', function () use ($app) {
    return $app['twig']->render('produkte.twig');
})->bind('produkte');

$app->get('/impressum', function () use ($app) {
    return $app['twig']->render('impressum.twig');
})->bind('impressum');



$app->run();
